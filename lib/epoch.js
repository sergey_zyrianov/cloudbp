function Epoch(args) {
    // cb, hosts, execStack, popCallback, mdl, twoWorkersPerHost
   this.hosts = args.hosts;
   this.cb = args.cb;
   this.popCallback = args.popCallback;
   this.execStack = args.execStack;
   console.log('Epoch on %d hosts'.grey, this.hosts.length);
   this.trainSetSize = 10748; // 19;
   this.batchSize = 128 / this.hosts.length; // 4;
   console.log('Batch size to be %s', this.batchSize);
   this.mdl = args.mdl === true;
   this.twoWorkersPerHost = args.twoWorkersPerHost === true;
   this._push();
   this.cb(0, 'OK');
}
Epoch.prototype = {
    _push : function () {
        var aggStepName = this.twoWorkersPerHost ? 'agg2w' : 'agg';
        for (var i=0; i < Math.floor(this.trainSetSize / this.batchSize); i += this.hosts.length) {
            console.log('batchNum %s', i / this.hosts.length);
            this.execStack.push({op:'downloadseed', cb:this.popCallback});
            this.execStack.push({op:'uploadseed', cb:this.popCallback});
            this.execStack.push({op:aggStepName, cb:this.popCallback});
            this.execStack.push({op:'batch',
                cb:this.popCallback,
                arg:{batchNum:i/this.hosts.length, batchSize : this.batchSize,
                  trainSize:this.trainSetSize, numHosts: this.hosts.length, mdl : this.mdl ? true : false}
            });
        }
    },
}

exports.exec = function(args) {
// cb, hosts, execStack, popCallback, mdl
    new Epoch(args);
}