
var D = require('JQDeferred');
var fbutil = require('./fbutil');
var job = require('./JobMonitor');
function PathMonitor(firebaseUrl, path) {
   this.firebaseUrl = firebaseUrl;
   this.ref = fbutil.fbRef(firebaseUrl, path.path);
   console.log('Looking after job in path "%s"'.grey, fbutil.pathName(this.ref));
   this.filter = path.filter || function() { return true; };
   this.parse  = path.parse || function(data) { return data; };
   this.hostId = path.hostId;

   this._init();
}

PathMonitor.prototype = {
   _init: function() {
      this.ref.on('child_added', this._process.bind(this, this._childAdded));
//      this.ref.on('child_changed', this._process.bind(this, this._childChanged));
//      this.ref.on('child_removed', this._process.bind(this, this._childRemoved));
   },

   _process: function(fn, snap) {
      var dat = snap.val();
      if( this.filter(dat) ) {
         fn.call(this, snap.name(), this.parse(dat));
      }
   },

   _childAdded: function(key, data) {
        job.process(this.firebaseUrl, [{path: fbutil.pathName(this.ref)  + '/'+ key, hostId: this.hostId}]);
        console.log('added pm key %s', key);
   },
   _childChanged: function(key, data) {
   },

};

exports.process = function(firebaseUrl, paths) {
   paths && paths.forEach(function(pathProps) {
      new PathMonitor(firebaseUrl, pathProps);
   });
};
