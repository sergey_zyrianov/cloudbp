function AggregatorPairSameHost(cb, left, right) {
   this.left = left;
   this.right = right;
   this.cb = cb;
   console.log('Aggregator pair on same host left=%s right=%s'.grey, this.left.id, this.right.id);
   // upload from right hosts
   // download to left hosts
   // agg
   this._agg();

}
AggregatorPairSameHost.prototype = {
    _agg : function() {
        console.log('./rnnagg %s.save %s.save'.grey, this.right.id, this.left.id);
        this.left.host.agg(this.cb, '../cloudbp' + this.right.id + '/' + this.right.id + '.save');
//        this.cb(this.left.id, 'OK')
    },

}

exports.exec = function(cb, left, right) {
    new AggregatorPairSameHost(cb, left, right);
}