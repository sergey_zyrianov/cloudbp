var ap = require('./aggpair');
var apsh = require('./aggpairsamehost');
function Aggregator2WorkersPerHost(cb, hosts) {
   this.hosts = hosts;
   this.cb = cb;
   console.log('Aggregator to sum results from %d hosts'.grey, hosts.length);
   this.rounds = 0;
   var c = this.hosts.length;
   while (c != 0) {
        c >>= 1;
        this.rounds++;
   }
   console.log('In %d rounds'.grey, this.rounds);
   this.roundIx = 1;
   this.reportCount = 0;
   this.buddy = 1;
   this._round();
}
Aggregator2WorkersPerHost.prototype = {
    _round : function () {
        console.log('round number %d gap=%d'.green, this.roundIx++, this.buddy);
        for (i=0; i + this.buddy < this.hosts.length;) {
            var l = this.hosts[i];
            var r = this.hosts[i + this.buddy];
            i += this.buddy * 2;
            if (l.id + 1 === r.id) {
                apsh.exec(this._cb.bind(this), l, r);
            } else {
                ap.exec(this._cb.bind(this), l, r);
            }
        }
        if (this.buddy >= this.hosts.length) {
            this.cb(this.hosts[0].id, 'OK');
        }
    },
    _cb : function (hostid, result) {
        if (result !== 'OK') {
            this.cb(hostid, result);
        }
        this.reportCount ++;
        if (this.reportCount >= (this.hosts.length >> this.buddy)) {
            console.log('next round due to reports %d >= %d'.cyan, this.reportCount, this.hosts.length >> this.buddy);
            this.buddy <<= 1;
            this.reportCount = 0;
            this._round();
        }

    }

}

exports.exec = function(cb, hosts) {
    new Aggregator2WorkersPerHost(cb,hosts);
}