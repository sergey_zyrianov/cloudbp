var fbutil = require('./fbutil');
var job = require('./JobMonitor');
function Host(firebaseUrl, hostId, jobDate, dataBin) {
   this.firebaseUrl = firebaseUrl;
   this.hostId = hostId;
   this.jobDate = jobDate;
   this.dataBin = dataBin;
   console.log("created hostid(%s) dataBin(%s)", this.hostdId, this.dataBin);
}
Host.prototype = {
    getRnnlib : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'getrnnlib'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): getRnnlib status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    getRnnlibLongerAbet : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'getrnnliblongerabet'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): getRnnlibLongerAbet status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    seed : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'seed'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): seed status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    uploadLastSave : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'uploadlastsave', bucket:this.jobDate, fileName : this.hostId+'.save'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): uploadLastSave status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    uploadAsSeed : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'uploadlastsave', bucket:this.jobDate, fileName:'seed.save'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): uploadLastSave status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    validate : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'validate', bucket:this.jobDate, params: '--dataBin=' + this.dataBin + " " + this.hostId + '.save'});
//        ref.child('status').on('value', function(snap) {
        ref.on('value', function(snap) {
            var v = snap.val();
            if (v == null || v['status'] === undefined) {
                return;
            }
            var status = v['status'];
            if (status !== 'OK') {
                cb(this.hostId, status);
            }
            var err = v['err'];
            if (err === undefined) {
                console.log('wait for err value set for host %s', this.hostId);
                return;
            }
            console.log('host(%s): validate status: %s', this.hostId, snap.val());
            cb(this.hostId, status, err);
        }.bind(this));
    },
    epoch : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'epoch', bucket:this.jobDate, params:'--dataFraction=1 --trainer_epoch=0 --totalEpochs=1 --dataBin=' + this.dataBin + ' ' + this.hostId + '.save'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): epoch status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    epochmdl : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'epoch', bucket:this.jobDate, params:'--mdl=true --dataFraction=1 --trainer_epoch=0 --totalEpochs=1 --dataBin=' + this.dataBin + ' ' + this.hostId + '.save'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): epoch status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    agg : function (cb, otherFile) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'agg', bucket:this.jobDate, params:this.hostId + '.save ' + otherFile});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): agg status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    downloadSeed : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'download', bucket:this.jobDate, fileName: 'seed.save', SaveAs: this.hostId + '.save'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): downloadSeed status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    downloadFile : function (cb, fileName) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'download', bucket:this.jobDate, fileName: fileName});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): downloadFile status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    saveAsBest : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'saveasbest', bucket:this.jobDate});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): saveAsBest status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },

    cleanupFb : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp');
        ref.remove();
        cb(this.hostId, 'OK');
    },
    createBucket : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'createbucket', bucket:this.jobDate});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): createBucket status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    deleteBuckets : function (cb) {
        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'deletebuckets', bucket:this.jobDate});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): deleteBuckets status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },
    batch : function (cb, arg) {
        //
        var batchNum = arg['batchNum'];
        var batchSize = arg['batchSize'];
        var trainSize = arg['trainSize'];
        var numHosts = arg['numHosts'];
        var mdl = arg['mdl'] === true ? true : false;
        var batchBin = '';
        var allHostsZero = this.dataBin.replace(/1/g, '0');
        var numBins = Math.floor(trainSize / batchSize);
        var hostBinOffset = batchNum * numHosts + this.hostId;
        if (hostBinOffset >= numBins) {
            hostBinOffset = numBins - 1;
        }

        for (var i = 0; i < numBins; i ++) {
            batchBin += '' + (i === hostBinOffset ? '1' : 0);
        }
        console.log('host(%s), batch(%s) %s', this.hostId, batchBin, hostBinOffset);


        var ref = fbutil.fbRef(this.firebaseUrl, 'cloudbp/' + this.hostId + '/' + this.jobDate);
        ref = ref.push({type:'epoch', bucket:this.jobDate, params:'--mdl=' + mdl + ' --verbose=true --dataFraction=1 --trainer_epoch=0 --totalEpochs=1 --dataBin=' + batchBin + ' ' + this.hostId + '.save'});
        ref.child('status').on('value', function(snap) {
            if (snap.val() == null) {
                return;
            }
            console.log('host(%s): epoch status: %s', this.hostId, snap.val());
            cb(this.hostId, snap.val());
        }.bind(this));
    },

}

exports.init = function(firebaseUrl, jobDate, hosts) {
    hosts.forEach(function (h) {
        h.host = new Host(firebaseUrl, h.id, jobDate, h.dataBin);
    });
}

