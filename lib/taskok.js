function TaskOkStub(fbRef, taskId, taskDef, hostId) {
    this.fbRef = fbRef;
    this.taskId = taskId;
    this.taskDef = taskDef;
    this.hostId = hostId;
    console.log('id %s def %s bucket %s', this.taskId,  this.taskDef, this.taskDef.bucket);
    console.log('task type %s taskId=%s'.green, this.taskDef.type, this.taskId);
    this.err = 'OK';
    this._status();
}
TaskOkStub.prototype = {
   _status: function() {
        console.log('updating status for task %s to %s', this.taskId, this.err);
        this.fbRef.child(this.taskId).update({status:this.err});
   },
};
exports.exec = function(fbRef, taskId, taskDef, hostId) {
    new TaskOkStub(fbRef, taskId, taskDef, hostId);
}