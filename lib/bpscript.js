var agg = require('./agg');
var agg2w = require('./agg2w');
var val = require('./val');
var epoch = require('./epoch');

function BPScript(hosts, execStack) {
   this.hosts = hosts;
   this.stepCompleteCount = 0;
   this.bestErr = 1e30;
   this.execStack = execStack;
   console.log('BPScript will control %d hosts'.grey, hosts.length);
}

BPScript.prototype = {
    _initStep: function(cb) {
       this.stepCompleteCount = 0;
       this.stepCb = cb;
    },
    _checkStepComplete: function(hostId, result) {
        if (result !== 'OK') {
            this.stepCb('host ' + hostId + ':' + result);
            return;
        }
        this.stepCompleteCount++;
        if (this.stepCompleteCount == this.hosts.length) {
            this.stepCb(result);
        }
    },
    _checkValidateComplete: function(hostId, result, err) {
        if (result !== 'OK') {
            this.stepCb('host ' + hostId + ':' + result);
            return;
        }
        err = parseFloat(err) / this.hosts.length;
        console.log("current err %s new err %s", this.bestErr, err);
        if (parseFloat(this.bestErr) > parseFloat(err) ) {
            console.log('got new best network with err=%s', err);
            this.bestErr = err;
            var subscript = new BPScript(this.hosts);
            subscript.saveasbest(function (result) {
                if (result !== 'OK') {
                    this.stepCb(result);
                    return;
                }
                subscript.uploadseed(function (result) {
                    if (result !== 'OK') {
                        this.stepCb(result);
                        return;
                    }
                    subscript.downloadseed(this.stepCb.bind(this));
                }.bind(this));
            }.bind(this));
            return;
        }
        this.stepCb(result);
    },
    getrnnlib: function (cb) {
        this._initStep(cb);
        this.hosts.forEach(function (h) {
            h.host.getRnnlib(function(hostid, result){
                this._checkStepComplete(hostid, result);
            }.bind(this));
        }.bind(this));
    },
    getrnnliblongerabet: function (cb) {
        this._initStep(cb);
        this.hosts.forEach(function (h) {
            h.host.getRnnlibLongerAbet(function(hostid, result){
                this._checkStepComplete(hostid, result);
            }.bind(this));
        }.bind(this));
    },
    seed: function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        this.hosts[0].host.seed(function(hostid, result){
            this._checkStepComplete(hostid, result)
        }.bind(this));
    },
    uploadseed: function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        this.hosts[0].host.uploadAsSeed(function(hostid, result){
            this._checkStepComplete(hostid, result)
        }.bind(this));
    },
    downloadseed: function (cb) {
        this._initStep(cb);
        this.hosts.forEach(function(h) {
            h.host.downloadSeed(function(hostid, result){
                this._checkStepComplete(hostid, result);
            }.bind(this));
        }.bind(this));
    },
    cleanupfb : function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        this.hosts[0].host.cleanupFb(function(hostid, result){
            this._checkStepComplete(hostid, result)
        }.bind(this));
    },
    createbucket : function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        this.hosts[0].host.createBucket(function(hostid, result){
            this._checkStepComplete(hostid, result)
        }.bind(this));
    },
    deletebuckets : function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        this.hosts[0].host.deleteBuckets(function(hostid, result){
            this._checkStepComplete(hostid, result)
        }.bind(this));
    },
    val0 : function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        this.hosts[0].host.validate(function(hostid, result){
            this._checkStepComplete(hostid, result)
        }.bind(this));
    },
    val : function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        val.exec(this._checkValidateComplete.bind(this), this.hosts);
    },
    saveasbest : function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        this.hosts[0].host.saveAsBest(function(hostid, result){
            this._checkStepComplete(hostid, result)
        }.bind(this));
    },
    epochorg: function (cb) {
        this._initStep(cb);
        this.hosts.forEach(function(h) {
            h.host.epoch(function(hostid, result){
                this._checkStepComplete(hostid, result);
            }.bind(this));
        }.bind(this));
    },
    agg: function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        agg.exec(this._checkStepComplete.bind(this), this.hosts);
    },
    agg2w: function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        agg2w.exec(this._checkStepComplete.bind(this), this.hosts);
    },
    nop: function (cb) {
        this._initStep(cb);
        cb('OK');
    },
    epoch: function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        // cb, hosts, execStack, popCallback, mdl
        var params = {
            cb : this._checkStepComplete.bind(this),
            hosts: this.hosts,
            execStack:  this.execStack,
            popCallback: cb,
            mdl: false
        };
        epoch.exec(params);
    },
    epoch2w: function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        // cb, hosts, execStack, popCallback, mdl
        var params = {
            cb : this._checkStepComplete.bind(this),
            hosts: this.hosts,
            execStack:  this.execStack,
            popCallback: cb,
            twoWorkersPerHost: true,
            mdl: false
        };
        epoch.exec(params);
    },
    epochmdl: function (cb) {
        this._initStep(cb);
        this.stepCompleteCount = this.hosts.length - 1;
        var params = {
            cb : this._checkStepComplete.bind(this),
            hosts: this.hosts,
            execStack:  this.execStack,
            popCallback: cb,
            mdl: true
        };
        epoch.exec(params);
    },
    batch: function(cb, arg) {
        this._initStep(cb);
        this.hosts.forEach(function(h) {
            h.host.batch(function(hostid, result){
                this._checkStepComplete(hostid, result);
            }.bind(this), arg);
        }.bind(this));
    }


}

exports.init = function (hosts, execStack) {
    return new BPScript(hosts, execStack);
}