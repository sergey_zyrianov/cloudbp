var AWS = require('aws-sdk');
var s3 = new AWS.S3();
var fs = require('fs');

function Uploader(cb, params) {
   this.params = params;
   this.cb = cb;
   this.params = params;
   this.retryCount = 0;
   this.err = 'OK';

   this._upload();

}
Uploader.prototype = {
    _upload : function () {
       console.log('Upload file %s to bucket=%s as %s'.grey, this.params.diskFileName, this.params.bucket, this.params.fileName);
        var bodyStream = fs.createReadStream(this.params.diskFileName);
        var obj = this;
        this.err = 'OK';
        var size = fs.statSync(this.params.diskFileName)['size'];
        console.log('file size is %s', size);
        var params = {Bucket: this.params.bucket, Key: this.params.fileName, Body: bodyStream, Metadata : {size: '' + size}};
        s3.putObject(params, function(err) {
            if (err) {
                console.log('retry after err %s for file %s', err, this.params.diskFileName)
                if (this.retryCount++ > 10) {
                    this.cb(err);
                    return;
                }
                this._upload();
                return;
            }
            this.cb('OK');
        }.bind(this));
    },
}
exports.init = function(cb, params) {
    new Uploader(cb, params);
}

