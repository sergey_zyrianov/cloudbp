function Validator(cb, hosts) {
   this.hosts = hosts;
   this.cb = cb;
   console.log('Validator to sum results from %d hosts'.grey, hosts.length);
   this.reportCount = 0;
   this.err = 0;
   this._start();
}
Validator.prototype = {
    _start : function () {
        for (i=0; i < this.hosts.length; ++i) {
            var h = this.hosts[i].host;
            h.validate(this._cb.bind(this));
        }
    },
    _cb : function (hostid, result, err) {
        if (result !== 'OK') {
            this.cb(hostid, result);
            return;
        }
        if (err == null) {
            this.cb(hostid, 'bad err');
            return;
        }
        console.log('host(%s) validation err=%s', hostid, err);
        this.reportCount ++;
        this.err = parseFloat(this.err) +  parseFloat(err);
        if (this.reportCount >= (this.hosts.length)) {
            console.log('validation completed, err=%s', err);
            this.cb(hostid, result, this.err);
        }

    }

}

exports.exec = function(cb, hosts) {
    new Validator(cb, hosts);
}