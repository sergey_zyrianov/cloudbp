
var D = require('JQDeferred');
var fbutil = require('./fbutil');
var conf = require('../config');
var task = require('./' + conf.WORKER_NAME);
function JobMonitor(firebaseUrl, path) {
   console.log('job url %s path %s', firebaseUrl, path.path);
   this.firebaseUrl = firebaseUrl;
   this.ref = fbutil.fbRef(firebaseUrl, path.path);
   console.log('Looking for job in path "%s"'.grey, fbutil.pathName(this.ref));
   this.filter = path.filter || function() { return true; };
   this.parse  = path.parse || function(data) { return data; };
   this.hostId = path.hostId;

   this._init();
}

JobMonitor.prototype = {
   _init: function() {
      this.ref.on('child_added', this._process.bind(this, this._childAdded));
//      this.ref.on('child_changed', this._process.bind(this, this._childChanged));
//      this.ref.on('child_removed', this._process.bind(this, this._childRemoved));
   },

   _process: function(fn, snap) {
      var dat = snap.val();
      if( this.filter(dat) ) {
         fn.call(this, snap.name(), this.parse(dat));
      }
   },

   _childAdded: function(key, data) {
        console.log('added key %s and data %s', key, data);
        task.exec(this.ref, key, data, this.hostId);
   },
   _childChanged: function(key, data) {
//        task.exec(this.ref, key, data);
   },

};

exports.process = function(firebaseUrl, paths) {
   paths && paths.forEach(function(pathProps) {
      new JobMonitor(firebaseUrl, pathProps);
   });
};
