var fbutil = require('./fbutil');
var AWS = require('aws-sdk');
var s3 = new AWS.S3();
var fs = require('fs');
var bd = require('./bdnldr');
var upldr = require('./upldrd.js');
var synthConfig = 'task prediction\n' +
                  'hiddenType lstm1d,lstm1d,lstm1d\n' +
                  'trainFile online.nc\n' +
                  'valFile online_validation.nc\n' +
                  'dataFraction 1\n' +
                  'maxTestsNoBest 20\n' +
                  'hiddenSize 400,400,400\n' +
                  'learnRate 1e-4\n' +
                  'momentum 0.9\n' +
                  'optimiser rmsprop\n' +
                  'verbose false\n' +
                  'predictionSteps 10\n' +
                  'bidirectional false\n' +
                  'autosave true\n' +
                  'loadWeights false\n' +
                  'mixtures 20 \n' +
                  'charWindowSize 10\n';
var aggConfig = 'task prediction\n' +
                  'hiddenType lstm1d,lstm1d,lstm1d\n' +
                  'trainFile online.nc\n' +
                  'valFile online_validation.nc\n' +
                  'dataFraction 1\n' +
                  'maxTestsNoBest 20\n' +
                  'hiddenSize 400,400,400\n' +
                  'learnRate 1e-4\n' +
                  'momentum 0.9\n' +
                  'optimiser rmsprop\n' +
                  'verbose false\n' +
                  'predictionSteps 10\n' +
                  'bidirectional false\n' +
                  'autosave true\n' +
                  'loadWeights true\n' +
                  'mixtures 20 \n' +
                  'charWindowSize 10\n';

function Task(fbRef, taskId, taskDef, hostId) {
    this.fbRef = fbRef;
    this.taskId = taskId;
    this.taskDef = taskDef;
    this.hostId = hostId;
    console.log('id %s def %s bucket %s', this.taskId,  this.taskDef, this.taskDef.bucket);
    console.log('task type %s taskId=%s'.green, this.taskDef.type, this.taskId);
    this.err = 'OK';
    this['_' + this.taskDef.type]();
}
Task.prototype = {
   _status: function() {
        console.log('updating status for task %s to %s', this.taskId, this.err);
        this.fbRef.child(this.taskId).update({status:this.err});
   },
   _undefined: function() {
        this.err = 'undefined task type for ' + this.taskId;
        this._status();
   },
   _renameLastResult: function() {
        var files = fs.readdirSync('.');
        var found = false;
        files.forEach( function (file) {
            if (file.match(/.last.save/) || file.match(/agg.save/)) {
                console.log("keep %s as %s.save".cyan, file, this.hostId);
                fs.rename(file, this.hostId+'.save');
                found = true;
                return;
            }
            if (file.match(/@/)) {
                console.log("will remove %s".red, file);
                fs.unlink(file);
            }
        }.bind(this));
        if (!found) {
            this.err = "last.save file not found - cant rename";
        }

   },
   _seed: function() {
        console.log('doing seed');
        console.log(fbutil.pathName(this.fbRef));
        fs.writeFile("synth.config", synthConfig);

        var sys = require('sys')
        var exec = require('child_process').execFile;
        var obj = this;
        function puts(error, stdout, stderr) {
            console.log(stdout); sys.puts(stdout);
            this._renameLastResult();
            this._status();
        }
        exec("./rnnlib", "--dataFraction=0 --autosave=true --totalEpochs=1 synth.config".split(/[\s,]+/), puts.bind(this));
   },
   _epoch: function() {
        console.log('doing local iteration');
        console.log(fbutil.pathName(this.fbRef));
        var sys = require('sys')
        var exec = require('child_process').execFile;
        var obj = this;
        function puts(error, stdout, stderr) {
            console.log(stdout); sys.puts(stdout);
            this._renameLastResult();
            this._status();
        }
        exec("./rnnlib", this.taskDef.params.split(/[\s,]+/), puts.bind(this));
   },
   _agg: function() {
        console.log('doing local iteration');
        console.log(fbutil.pathName(this.fbRef));
        var sys = require('sys')
        var exec = require('child_process').execFile;
        var obj = this;
        function puts(error, stdout, stderr) {
            console.log(stdout); sys.puts(stdout);
            fs.appendFileSync('agg.save', aggConfig);
            this._renameLastResult();
            this._status();
        }
        exec("./rnnagg", this.taskDef.params.split(/[\s,]+/), puts.bind(this));
   },
   _validate: function() {
        console.log('doing validate');
        console.log(fbutil.pathName(this.fbRef));
        fs.writeFile("synth.config", synthConfig);

        var sys = require('sys')
        var exec = require('child_process').execFile;
        var obj = this;
        function puts(error, stdout, stderr) {
            console.log(stdout);
            var n = stdout.match(/validation errors:(.*)$/gmi);
            console.log('store error in FB:%s', n);
            n = '' + n;
            n = n.substring(n.search(':') + 1);
            if (isNaN(parseFloat(n))) {
                this.err = 'failed to find validation error';
            } else {
                this.fbRef.child(this.taskId).update({err:n});
            }

            this._status();
        }
        exec("./rnnval", this.taskDef.params.split(/[\s,]+/), puts.bind(this));
   },
   _saveasbest: function() {
        var sys = require('sys')
        var exec = require('child_process').exec;
        exec("cp " + this.hostId + ".save bestnet.save", function(error, stdout, stderr) {
            if (error) {
                this.err = error;
            }
            this._status();
        }.bind(this));
   },
   _uploadlastsave: function() {
        var files = fs.readdirSync('.');
        var foundFile = false;
        var obj = this;
        this.err = 'last.save file not found';
        files.forEach(function(f){
            if (!f.match(this.hostId + '.save')) {
                return;
            }
            console.log('uploading %s as %s to %s bucket', f, this.taskDef.fileName, this.taskDef.bucket);
            foundFile = true;
            this.err = 'OK';
            var params = {bucket: this.taskDef.bucket, fileName: this.taskDef.fileName, diskFileName:f};
            upldr.init(function(err) {
                if (err !== 'OK') {
                    this.err = err;
                }
                obj._status();
            }.bind(this), params);
        }.bind(this));
        if (!foundFile) {
            this._status();
        }
   },
   _getrnnlib: function (bucketName) {
        if (typeof bucketName === 'undefined') {
            bucketName = 'rnnlib';
        }
        var binFolder = /^darwin/.test(process.platform) ? 'mac/' : '';
        var params = [{Bucket: bucketName , Key: binFolder + 'rnnlib', Exec: true, SaveAs : 'rnnlib'},
                      {Bucket: bucketName , Key: binFolder + 'rnnagg', Exec: true, SaveAs : 'rnnagg'},
                      {Bucket: bucketName , Key: binFolder + 'rnnval', Exec: true, SaveAs : 'rnnval'},
                      {Bucket: bucketName , Key: binFolder + 'rnnsynth', Exec: true, SaveAs : 'rnnsynth'},
                      {Bucket: bucketName, Key: 'online.nc'},
                      {Bucket: bucketName, Key: 'online_validation.nc'},
                      {Bucket: bucketName, Key: 'prime.nc'},
                      {Bucket: bucketName, Key: 'bestnet.save'},
                      {Bucket: bucketName, Key: 'bestnet.1d.save.1100'},
                      {Bucket: bucketName, Key: 'inkposter.conf'},
                      {Bucket: bucketName, Key: 'toster-1.0-SNAPSHOT.one-jar.jar'},
                      {Bucket: bucketName, Key: 'online_validation.nc'},
        ];
        bd.getMany(function(err) {
            this.err = err;
            this._status();
        }.bind(this), params);
   },
   _getrnnliblongerabet: function () {
        this._getrnnlib('rnnliblongerabet')
   },
   _download: function () {
        var params = [{Bucket: this.taskDef.bucket, Key: this.taskDef.fileName, SaveAs: this.taskDef.SaveAs}];
        bd.getMany(function(err) {
            this.err = err;
            this._status();
        }.bind(this), params);
   },
   _createbucket : function (err) {
        var s3bucket = new AWS.S3({params: {Bucket: this.taskDef.bucket}});
        s3bucket.createBucket(function(err) {
            if (err) {
                this.err = err;
            }
            this._status();
        }.bind(this));
   },
   _deletebuckets : function (err) {
        s3.listBuckets({}, function (err, data) {
            var buckets = data.Buckets;

            buckets.forEach(function (b) {
                if (b.Name.substr(0, 4) === this.taskDef.bucket.substr(0,4)) {
                    console.log('will delete bucket %s'.red, b.Name);
                    s3.listObjects({Bucket: b.Name}, function(err, data){
                         if (err) {
                            console.log("error listing bucket objects "+err);
                            return;
                         }
                         console.log('clear bucket %s %s', b.Name, data.Contents.length);
                         if (data.Contents.length == 0) {
                            s3.deleteBucket({Bucket: b.Name}, function(err){
                                if(err) console.log('cant delete bucket %s: %s', b.Name, err);
                            });
                            return;
                         }
                         var items = [];
                         data.Contents.forEach(function(i){
                            items.push({Key: i.Key});
                         });
                         s3.deleteObjects({Bucket:b.Name, Delete: {Objects: items}},
                            function(err) {
                                if (err) {
                                    console.log("error deleting bucket objects %s".red, err);
                                    return;
                                }
                                s3.deleteBucket({Bucket: b.Name}, function(err){
                                    if (err)console.log('error deleting bucket %s', err);
                                });

                            });
                    });
                }
            }.bind(this));
            this._status();

        }.bind(this));
        var s3bucket = new AWS.S3({params: {Bucket: this.taskDef.bucket}});
        s3bucket.createBucket(function(err) {
            if (err) {
                this.err = err;
            }
            this._status();
        }.bind(this));
   },
};
exports.exec = function(fbRef, taskId, taskDef, hostId) {
    new Task(fbRef, taskId, taskDef, hostId);
}