var AWS = require('aws-sdk');
var s3 = new AWS.S3();
var fs = require('fs');

function Downloader(cb, params) {
   this.params = params;
   this.cb = cb;
   this.fileName = this.params.SaveAs ? this.params.SaveAs : this.params.Key;
   this.retryCount = 0;

   console.log('Downloader from bucket=%s item=%s as %s'.grey, this.params.Bucket, this.params.Key, this.fileName);
   this._getsize();

}
Downloader.prototype = {
    _getsize : function () {
        s3.headObject({Bucket:this.params.Bucket, Key:this.params.Key}, function(err, data) {
            console.log('heading %s %s', err, data);
            if (!data) {
                this.cb('Cant get metadata from S3 for ' + this.params.Key);
                return;
            }
            if (!data['Metadata'] && /.*save$/.test(this.fileName))  {
                this.cb('Cant get size from metadata from S3 for ' + this.paramsKey);
                return;
            }
            if (!data['Metadata']) {
                this.expectedSize = 0;
            } else {
                this.expectedSize = data['Metadata']['size'];
            }
            this._download();
        }.bind(this));
    },

    _download : function () {
        if (false && fs.existsSync(fileName)) {
            console.log('already have %s'.cyan, fileName);
            this.cb('OK');
            return;
        }
        var file = require('fs').createWriteStream(this.fileName);
        file
        .on('finish',
            function () {
                console.log('expected file size %d for %s', this.expectedSize, this.fileName);
                if (this.expectedSize && this.expectedSize != fs.statSync(this.fileName)['size']) {
                    console.log('%s bad file size %d', this.fileName, fs.statSync(this.fileName)['size']);
                    this.retryCount++;
                    if (this.retryCount < 10) {
                        console.log('will retry, retry count=%d for %s', this.retryCount, this.fileName);
                        this._getsize();
                        return;
                    }
                    this.cb('Did not get all bytes: expected ' + this.expectedSize);
                    return;
                }
                if(this.params.Exec)
                    fs.chmodSync(this.fileName, '755');
                this.cb('OK');

            }.bind(this))
        .on('error',
            function(err){
                this.retryCount++;
                if (this.retryCount < 10) {
                    console.log('will retry, retry count=%d for %s', this.retryCount, this.fileName);
                    this._download();
                    return;
                }
                console.log('err=%s'.red, err);
                this.cb(err);
            }.bind(this));

        s3.getObject({Bucket:this.params.Bucket, Key:this.params.Key}).createReadStream().pipe(file);
    }
}
exports.init = function(cb, params) {
    new Downloader(cb, params);
}


