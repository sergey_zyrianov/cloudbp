var d = require('./dnldr');

function BatchDownloader(cb, params, seq) {
   this.params = params;
   this.cb = cb;
   this.countFiles = params.length;
   this.countDoneFiles = 0;
   this.sequential = seq;
   if (seq) {
    d.init(this._cbSeq.bind(this), params[0]);
    return;
   }
   params.forEach(function(p) {
        d.init(this._cb.bind(this), p);
   }.bind(this));

}
BatchDownloader.prototype = {
    _cb : function (err) {
        if (err !== 'OK') {
            this.cb(err);
            return;
        }
        this.countDoneFiles++;
        if (this.countDoneFiles == this.countFiles) {
            this.cb('OK');
            return;
        }
    },
    _cbSeq : function (err) {
        if (err !== 'OK') {
            this.cb(err);
            return;
        }
        this.countDoneFiles++;
        if (this.countDoneFiles == this.countFiles) {
            this.cb('OK');
            return;
        }
        d.init(this._cbSeq.bind(this), this.params[this.countDoneFiles]);
    }
}
exports.getMany = function(cb, params) {
    new BatchDownloader(cb, params);
}
exports.getOne = function(cb, params) {
    new BatchDownloader(cb, [].push(params));
}
exports.getManyOneByOne = function(cb, params) {
    new BatchDownloader(cb, params, true);
}

