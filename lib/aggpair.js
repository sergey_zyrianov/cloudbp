function AggregatorPair(cb, left, right) {
   this.left = left;
   this.right = right;
   this.cb = cb;
   console.log('Aggregator pair left=%s right=%s'.grey, this.left.id, this.right.id);
   // upload from right hosts
   // download to left hosts
   // agg
   this._uploadRight();

}
AggregatorPair.prototype = {
    _uploadRight : function () {
        console.log('upploading from %s'.cyan, this.right.id);
        this.right.host.uploadLastSave(this._cbUploadRight.bind(this));
    },
    _cbUploadRight : function(hostid, result) {
        if (result !== 'OK') {
            this.cb(hostid, result);
            return;
        }
        this._downloadLeft();
    },
    _downloadLeft : function () {
        console.log('downloading %s.save to %s'.grey, this.right.id, this.left.id);
        this.left.host.downloadFile(this._cbDownloadLeft.bind(this), this.right.id + '.save');
    },
    _cbDownloadLeft : function (id, result) {
        if (result !== 'OK') {
            this.cb(hostid, result);
            return;
        }
        this._agg();
    },
    _agg : function() {
        console.log('./rnnagg %s.save %s.save'.grey, this.right.id, this.left.id);
        this.left.host.agg(this.cb, this.right.id + '.save');
//        this.cb(this.left.id, 'OK')
    },

}

exports.exec = function(cb, left, right) {
    new AggregatorPair(cb, left, right);
}