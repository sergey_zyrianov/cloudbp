var util         = require('util');

console.log = function() {
    process.stdout.write((new Date()).toISOString().substring(0,19).replace('T', ' ')+': ');
    process.stdout.write(util.format.apply(this, arguments) + '\n');
};





/**
 * This config file is provided as a convenience for development. You can either
 * set the environment variables on your server or modify the values here.
 *
 * At a minimum, you must set FB_URL and Paths to Monitor. Everything else is optional, assuming your
 * ElasticSearch server is at localhost:9200.
 */

/** Firebase Settings
 ***************************************************/

// Your Firebase instance where we will listen and write search results
process.env.FB_NAME = process.env.FB_NAME || 'inkposter';
exports.FB_URL   = 'https://' + process.env.FB_NAME + '.firebaseio.com/';

// Either your Firebase secret or a token you create with no expiry, used to authenticate
// To Firebase and access search data.
exports.FB_TOKEN = process.env.FB_TOKEN || null;

// The path in your Firebase where clients will write search requests
exports.FB_REQ   = process.env.FB_REQ || 'test/request';

// The path in your Firebase where this app will write the results
exports.FB_RES   = process.env.FB_RES || 'test/response';
exports.HOST_ID = process.env.HOST_ID || 'testhost';
exports.HOST_COUNT = process.env.HOST_COUNT || 1;
exports.BPSCRIPT = process.env.BPSCRIPT || 'cleanupfb,createbucket,getrnnlib,seed,uploadseed,downloadseed,deletebuckets'
exports.DATA_FRACTION = process.env.DATA_FRACTION || 1;
exports.WORKER_NAME = process.env.WORKER_NAME || 'task';
exports.TASK = process.env.TASK || 'none';
exports.WORKERS_PER_HOST = process.env.WORKERS_PER_HOST || 2;


/** Paths to Monitor
 *
 * Each path can have these keys:
 * {string}   path:    [required] the Firebase path to be monitored, for example, `users/profiles`
 *                     would monitor https://<instance>.firebaseio.com/users/profiles
 * {string}   index:   [required] the name of the ES index to write data into
 * {string}   type:    [required] name of the ES object type this document will be stored as
 * {Array}    fields:  list of fields to be monitored (defaults to all fields)
 * {Function} filter:  if provided, only records that return true are indexed
 * {Function} parser:  if provided, the results of this function are passed to ES, rather than the raw data (fields is ignored if this is used)
 ****************************************************/

exports.paths = [
   {
      path:  'cloudbp/' + exports.HOST_ID,
      hostId: exports.HOST_ID,
   },
];
