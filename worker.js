#!/usr/bin/env node

var    conf          = require('./config'),
       fbutil        = require('./lib/fbutil'),
       PathMonitor   = require('./lib/PathMonitor');

fbutil.auth(conf.FB_URL, conf.FB_TOKEN).done(function() {
   PathMonitor.process(conf.FB_URL, conf.paths);
});
