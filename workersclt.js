#!/usr/bin/env node
var sys = require('sys');
var execSync = require("execSync");
var AWS = require('aws-sdk');
if (!execSync) {
    console.log("no execsync");
    return;
}
var ec2 = new AWS.EC2({region: 'us-east-1'}),
    conf  = require('./config');
var params = {
    Filters : [
                {
                    Name: 'tag:Name',
                    Values: [
                            'bpslave_m3'
                    ]
                }
              ]
};

function getWorkerIps(cb) {
    ec2.describeInstances(params, function(err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else
        {
            console.log(data.Reservations[0].Instances);           // successful response
            cb(data.Reservations[0].Instances);
        }
    });
}
var hosts = [];

function ssh(ip, cmd) {
    var line = 'ssh -i Lab.pem ubuntu@' + ip + ' "' + cmd + '"';
    console.log('%s', line);
    return execSync.exec(line).stdout;
}
function dirName(hostid) {
    return 'cloudbp' + hostid;
}
function cloneCloudbp(ip, hostid) {
    var cmd = '[ -d ' + dirName(hostid) + ' ] || ' +
      'git clone https://sergey_zyrianov@bitbucket.org/sergey_zyrianov/cloudbp.git ' +
      dirName(hostid);
    ssh(ip, cmd);
}

function getRnnlibLongerAbet(ip, hostid) {
    var cmd = 'cd ' + dirName(hostid) + ' && nodejs getrnnlongerabet.js';
    ssh(ip, cmd);
}
function removeNohup(ip, hostid) {
    var cmd = 'cd ' + dirName(hostid) + ' && rm nohup.out';
    ssh(ip, cmd);
}
function startWorker(ip, hostid) {
    var cmd = 'cd ' + dirName(hostid) + '; HOST_ID=' + hostid + ' nodejs worker.js </dev/null >nohup.out 2>&1 &';
    ssh(ip, cmd);
}
function workerGitPull(ip, hostid) {
    var cmd = 'cd ' + dirName(hostid) + ' && git pull';
    ssh(ip, cmd);
}
function nmpInstall(ip, hostid) {
    var cmd = 'cd ' + dirName(hostid) + ' && sudo npm install';
    ssh(ip, cmd);
}

var ctl = {
    none : function() {
        console.log('run with TASK=<task> nodejs workersctl.js');
        console.log('example kill nodejs on servers: TASK=killnodejs nodejs workersctl.js');
        console.log('start workers:TASK=killnodejs,rmnohup,start node workersclt.js');
        console.log('update git:TASK=killnodejs,gitpull node workersclt.js');
    },
    clone : function () {
        var hostid = 0;
        hosts.forEach(function(h) {
            for (var i=0; i < conf.WORKERS_PER_HOST; ++i) {
                cloneCloudbp(h, hostid++);
            }
        });
    },
    getrnnlib : function () {
        var hostid = 0;
        hosts.forEach(function(h) {
            for (var i=0; i < conf.WORKERS_PER_HOST; ++i) {
                getRnnlibLongerAbet(h, hostid++);
            }
        });
    },
    start : function () {
        var hostid = 0;
        hosts.forEach(function(h) {
            for (var i=0; i < conf.WORKERS_PER_HOST; ++i) {
                startWorker(h, hostid++);
            }
        });
    },
    rmnohup : function () {
        var hostid = 0;
        hosts.forEach(function(h) {
            for (var i=0; i < conf.WORKERS_PER_HOST; ++i) {
                removeNohup(h, hostid++);
            }
        });
    },
    npm : function () {
        var hostid = 0;
        hosts.forEach(function(h) {
            for (var i=0; i < conf.WORKERS_PER_HOST; ++i) {
                nmpInstall(h, hostid++);
            }
        });
    },
    gitpull : function () {
        var hostid = 0;
        hosts.forEach(function(h) {
            for (var i=0; i < conf.WORKERS_PER_HOST; ++i) {
                workerGitPull(h, hostid++);
            }
        });
    },
    killnodejs : function () {
        hosts.forEach(function(h) {
            ssh(h, 'killall nodejs')
        });
    }
};


function fillHostsIps(ips) {
    ips.forEach(function (i) {
            console.log("got ip: %s", i.PublicIpAddress );
            hosts.push(i.PublicIpAddress);
    });
    var tasks = conf.TASK.split(',');
    tasks.forEach(function(t) {
        ctl[t]();
    });
}


getWorkerIps(fillHostsIps);