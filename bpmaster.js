#!/usr/bin/env node

var    conf          = require('./config'),
       fbutil        = require('./lib/fbutil'),
       host        = require('./lib/Host'),
       PathMonitor   = require('./lib/PathMonitor'),
       bpscript     = require('./lib/bpscript');
var hosts = [];
var stepCompleteCounts = [];
var d = new Date();
var jobDate = '' + d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + '-' + d.getHours() +
    '-' + d.getMinutes() + '-' + d.getSeconds();
console.log('Job id is %s', jobDate);
var stepNames = conf.BPSCRIPT.split(',');
var execStack = [];
stepNames.reverse().forEach(function (n) {
    execStack.push({op:n, cb:opCallback});
});

var stepIndex = -1;
var BPS = bpscript.init(hosts, execStack);
var hostZero = '';
for (i=0; i < conf.HOST_COUNT; ++i) {
    hostZero += '0';
}
for (i=0; i < conf.HOST_COUNT; ++i) {
    var dataBin = hostZero.substr(0,i) + '1' + hostZero.substr(i+1);
    dataBin[i] = '1';
    hosts.push( {id: i, dataBin: dataBin});
    console.log('hostId=%s dataBin=%s', i, dataBin);
    stepCompleteCounts[i] = 0;
}

fbutil.auth(conf.FB_URL, conf.FB_TOKEN).done(function() {
    host.init(conf.FB_URL, jobDate, hosts);
    doStack('OK');
});
/*
* cloudbp/<host id>/<date>/<task id>/ status, type, params
*/
function doStack() {
    var obj = execStack.pop();
    if (obj === undefined) {
        done();
        process.exit(0);
        return;
    }
    console.log('doing step %s'.green, obj['op']);
    BPS[obj['op']](obj['cb'], obj['arg']);
}
function opCallback(result) {
    if (result !== 'OK') {
        console.log('step %d failed with error: %s'.red, stepIndex, result);
        process.exit(-1);
    }
    doStack();
}
function doBpscript(result) {
    if (result !== 'OK') {
        console.log('step %d failed with error: %s'.red, stepIndex, result);
        process.exit(-1);
    }
    stepIndex++;
    if (stepIndex == stepNames.length) {
        done();
        process.exit(0);
    }
    console.log('doing step %s'.green, stepNames[stepIndex]);
    BPS[stepNames[stepIndex]](doBpscript);
}

function done() {
    console.log("BP done");
}
